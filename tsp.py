from os import system, name


def clear():
    if name == 'nt':
        _ = system('cls')
    else:
        _ = system('clear')


def print_menu():
    print(f"Current filename: {filename}")
    print("1. Load from file")
    print("2. Show current loaded matrix")
    print("3. Solve TSP")
    print("5. Exit")
    print("Your choice: ", end='')


def load_from_file(filename):
    cities_matrix = []
    with open(str(filename)+'.txt') as file:
        number_of_cities = file.readline()
        tmp = file.read().splitlines()
        for line in tmp:
            costs = line.split(' ')
            cities_matrix.append(costs)
        return number_of_cities, cities_matrix


def print_cities_matrix(cities_matrix):
    for city_connections in cities_matrix:
        print(city_connections)
    print("press any key to contiune...")
    _ = input()


if __name__ == "__main__":
    filename = "not loaded yet"
    cities_matrix = []
    choice = None
    while (choice != '5'):
        clear()
        print_menu()
        choice = input()
        if choice == '1':
            print("Enter fil: ", end='')
            filename = input()
            numeber_of_cities, cities_matrix = load_from_file(filename)
        elif choice == '2':
            if cities_matrix:
                print_cities_matrix(cities_matrix)
            else:
                print("Not loaded yet")
        elif choice == '3':
            pass
